package com.example.publishlib

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.exaplelibrary.Example1
import com.example.exaplelibrary.Example2
import com.example.exaplelibrary.SOME_CONST
import com.example.mygoolecloudlibrary.LibFile

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Example1.SOME_CONST
        Example1.saySomething()

        Example2.saySomething()

        Log.d("TODEL", "SOME_CONST = " + SOME_CONST)
        Log.d("TODEL", "Example1.SOME_CONST  = " + Example1.SOME_CONST )
        Log.d("TODEL", "Example1.saySomething()  = " + Example1.saySomething() )
        Log.d("TODEL", "Example2.saySomething()  = " + Example2.saySomething() )


        LibFile.LIB_CONST
    }
}
